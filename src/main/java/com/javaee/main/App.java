package com.javaee.main;

import javax.swing.SwingUtilities;

import com.javaee.main.views.MainView;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		/*
		 * MainView mainView=new MainView();
		 * 
		 * mainView.setLocationRelativeTo(null); mainView.setSize(500, 600);
		 * mainView.setVisible(true);
		 */
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				MainView mainView = new MainView();
				mainView.start();
			}
		});
	}
}
