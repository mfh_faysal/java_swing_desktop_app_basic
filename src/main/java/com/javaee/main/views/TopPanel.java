package com.javaee.main.views;

import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.javaee.main.model.Data;
import com.javaee.main.model.DataDao;

import net.miginfocom.swing.MigLayout;

public class TopPanel extends JPanel {

	private static TableModel tableModel;
	private static JTable table;
	private static List<Data> dataList;
	private static TopPanel instance;
	private static JScrollPane scrollPane;

	public TopPanel() {
		initComponents();
	}

	private void initComponents() {

		setLayout(new MigLayout("fill"));

		tableModel = new TableModel<Data>(Data.class);

		// tableModel.addColumn("ID", "id");
		tableModel.addColumn("NAME", "name");
		tableModel.addColumn("ROLL", "roll");
		tableModel.addColumn("DEPT", "dept");
		tableModel.addColumn("SESSION", "session");

		table = new JTable(tableModel);
		table.setRowHeight(30);
		// table.setDefaultRenderer(Object.class, new
		// DefaultTableCellRenderer());

		scrollPane = new JScrollPane(table);

		DataDao dataDao = new DataDao();
		if (dataDao.createConnection() != null) {
			dataList = DataDao.getInstance().getAllData();

			tableModel.addRows(dataList);
		}
		add(scrollPane, "grow");

	}

	public static void refresh() {

		tableModel.removeAll();
		dataList = DataDao.getInstance().getAllData();
		tableModel.addRows(dataList);
		table.setModel(tableModel);
		table.repaint();

	}

	public static Data getSelectedTableRow() {
		int selectedRow = table.getSelectedRow();
		if (selectedRow < 0) {
			return null;
		}
		Data selectedData = (Data) tableModel.getRow(selectedRow);
		return selectedData;
	}

	public static TopPanel getInstance() {
		if (instance == null) {
			instance = new TopPanel();

		}
		return instance;
	}

}
