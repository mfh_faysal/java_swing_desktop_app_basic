package com.javaee.main.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.javaee.main.model.Data;
import com.javaee.main.model.DataDao;
/*import com.mysql.cj.core.util.StringUtils;*/
import com.mysql.jdbc.StringUtils;

import net.miginfocom.swing.MigLayout;

public class DataForm extends JDialog {

	private Data data;
	// private Data newData;
	private JTextField tfName;
	private JTextField tfRoll;
	private JTextField tfDept;
	private JTextField tfSession;
	private boolean isCancelled;
	private boolean isEditMode;

	public DataForm(Data data, boolean isEditMode) {
		super(MainView.getInstance(), ModalityType.APPLICATION_MODAL);

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(500, 300);
		setLocationRelativeTo(this);
		setResizable(false);
		setAlwaysOnTop(true);

		// setVisible(true);

		isCancelled = false;
		this.isEditMode = isEditMode;

		this.data = data;

		getDisplayText();

		initComponents();

		updateView();
	}

	private void getDisplayText() {

		if (data.getId() == 0) {
			setTitle("Add Data");
		} else {
			setTitle("Edit Data");
		}

	}

	private void initComponents() {

		setLayout(new MigLayout("fill"));

		JPanel panel = new JPanel(new MigLayout("fillx", "[][]50px", ""));

		JLabel lblName = new JLabel("Name");
		JLabel lblRoll = new JLabel("Roll");
		JLabel lblDept = new JLabel("Dept");
		JLabel lblSession = new JLabel("Session");

		tfName = new JTextField();
		tfRoll = new JTextField();
		tfDept = new JTextField();
		tfSession = new JTextField();

		panel.add(lblName, "cell 0 0, center ");
		panel.add(tfName, "cell 1 0, growx");
		panel.add(lblRoll, "cell 0 1, center");
		panel.add(tfRoll, "cell 1 1, growx");
		panel.add(lblDept, "cell 0 2, center");
		panel.add(tfDept, "cell 1 2, growx");
		panel.add(lblSession, "cell 0 3, center");
		panel.add(tfSession, "cell 1 3, growx");

		JPanel buttonPanel = new JPanel();

		JButton btnSave = new JButton("Save");
		JButton btnCancel = new JButton("Cancel");

		buttonPanel.add(btnSave);
		buttonPanel.add(btnCancel);

		btnSave.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				updateModel();

			}
		});

		btnCancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				isCancelled = true;
				dispose();

			}
		});

		add(panel, " center,growx,wrap");
		add(buttonPanel, " center,growx");

	}

	private void updateView() {
		tfName.setText(data.getName());
		tfRoll.setText(data.getRoll());
		tfDept.setText(data.getDept());
		tfSession.setText(data.getSession());

	}

	private void updateModel() {
		if (save()) {
			if (!isEditMode) {
				DataDao.getInstance().add(data);
				JOptionPane.showMessageDialog(this, "Successfully Saved!");
				this.dispose();
			} else {
				DataDao.getInstance().update(data);
				JOptionPane.showMessageDialog(this, "Successfully Updated!");
				this.dispose();
			}
		} else {
			JOptionPane.showMessageDialog(this, "Something Wrong!");
		}
	}

	private boolean save() {

		String name = tfName.getText();
		String roll = tfRoll.getText();
		String dept = tfDept.getText();
		String session = tfSession.getText();

		if (StringUtils.isNullOrEmpty(name)) {
			JOptionPane.showMessageDialog(this, "Please enter name!");
			return false;
		}
		if (StringUtils.isNullOrEmpty(roll)) {
			JOptionPane.showMessageDialog(this, "Please enter roll!");
			return false;
		}
		if (StringUtils.isNullOrEmpty(dept)) {
			JOptionPane.showMessageDialog(this, "Please enter dept!");
			return false;
		}
		if (StringUtils.isNullOrEmpty(session)) {
			JOptionPane.showMessageDialog(this, "Please enter session!");
			return false;
		}

		data.setName(name);
		data.setRoll(roll);
		data.setDept(dept);
		data.setSession(session);

		return true;
	}

	public Data getData() {
		return data;
	}

	public boolean isCancelled() {
		return isCancelled;
	}

}
