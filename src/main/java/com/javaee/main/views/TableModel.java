package com.javaee.main.views;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class TableModel<Data> extends AbstractTableModel {
	private List<Data> rows = new ArrayList<Data>();
	private List<BeanColumn> columns = new ArrayList<BeanColumn>();
	private Class<?> beanClass;

	public TableModel(Class<Data> beanClass) {
		this.beanClass = beanClass;
	}

	public void addColumn(String columnGUIName, String beanAttribute, EditMode editable) {
		try {
			PropertyDescriptor descriptor = new PropertyDescriptor(beanAttribute, beanClass);
			columns.add(new BeanColumn(columnGUIName, editable, descriptor));
		} catch (Exception e) {
		}
	}

	public void addColumn(String columnGUIName, String beanAttribute) {
		addColumn(columnGUIName, beanAttribute, EditMode.NON_EDITABLE);
	}

	public void addRow(Data row) {
		rows.add(row);
		fireTableDataChanged();
	}

	public void removeRow(Data row) {
		rows.remove(row);
		fireTableDataChanged();
	}

	public void removeRow(int index) {
		rows.remove(index);
		fireTableRowsDeleted(index, index);
	}

	public void removeAll() {
		rows.clear();
		fireTableDataChanged();
	}

	public void addRows(List<Data> rows) {
		if (rows == null) {
			return;
		}
		for (Data row : rows) {
			addRow(row);
		}
		fireTableDataChanged();

	}

	public int getColumnCount() {
		return columns.size();

	}

	public int getRowCount() {
		return rows.size();
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		BeanColumn column = columns.get(columnIndex);
		Data row = rows.get(rowIndex);

		Object result = null;
		try {
			result = column.descriptor.getReadMethod().invoke(row);
		} catch (Exception e) {
		}
		return result;
	}

	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		Data row = rows.get(rowIndex);
		BeanColumn column = columns.get(columnIndex);

		try {
			column.descriptor.getWriteMethod().invoke(row, value);
		} catch (Exception e) {
		}
	}

	public Class<Data> getColumnClass(int columnIndex) {
		BeanColumn column = columns.get(columnIndex);
		Class<Data> returnType = (Class<Data>) column.descriptor.getReadMethod().getReturnType();
		return returnType;
	}

	public String getColumnName(int column) {
		return columns.get(column).columnGUIName;
	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columns.get(columnIndex).editable == EditMode.EDITABLE;
	}

	public void setRow(int index, Data row) {
		getRows().set(index, row);
	}

	public Data getRow(int index) {
		return getRows().get(index);
	}

	public List<Data> getRows() {
		return rows;
	}

	public enum EditMode {
		NON_EDITABLE, EDITABLE;
	}

	/** One column in the table */
	private static class BeanColumn {
		private String columnGUIName;
		private EditMode editable;
		private PropertyDescriptor descriptor;

		public BeanColumn(String columnGUIName, EditMode editable, PropertyDescriptor descriptor) {
			this.columnGUIName = columnGUIName;
			this.editable = editable;
			this.descriptor = descriptor;
		}
	}

}