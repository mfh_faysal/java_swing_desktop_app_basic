package com.javaee.main.views;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class MainView extends JFrame {

	private static MainView instance;
	private ImageIcon applicationIcon;

	public MainView() {
		super("JAVA_FUN");
		applicationIcon = new ImageIcon(getClass().getResource("/images/user1.jpg"));
		setIconImage(applicationIcon.getImage());
	}

	public void start() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 600);
		setLocationRelativeTo(null);
		setResizable(false);
		setAlwaysOnTop(false);
		setVisible(true);

		initComponents();
	}

	private void initComponents() {

		setLayout(new BorderLayout());

		add(new TopPanel(), BorderLayout.NORTH);
		add(new BottomPanel(), BorderLayout.SOUTH);

	}

	public static MainView getInstance() {
		if (instance == null) {
			instance = new MainView();

		}
		return instance;
	}

}
