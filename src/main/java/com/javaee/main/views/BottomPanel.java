package com.javaee.main.views;

import java.awt.Dialog.ModalExclusionType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.javaee.main.model.Data;
import com.javaee.main.model.DataDao;

import net.miginfocom.swing.MigLayout;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JRViewer;

public class BottomPanel extends JPanel {

	public BottomPanel() {
		initComponents();
	}

	private void initComponents() {

		setLayout(new MigLayout("fill"));

		JPanel buttonPanel = new JPanel(new MigLayout("fillx"));

		JButton btnAdd = new JButton("Add");
		JButton btnEdit = new JButton("Edit");
		JButton btnDelete = new JButton("Delete");
		JButton btnReport = new JButton("Report");

		btnAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (DataDao.getInstance().createConnection() != null) {
					DataForm dataForm = new DataForm(new Data(), false);

					dataForm.setVisible(true);
					if (dataForm.isCancelled()) {
						return;
					}

					TopPanel.refresh();
				} else {
					JOptionPane.showMessageDialog(getParent(), "Database not connected!");
				}
			}
		});

		btnEdit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (TopPanel.getSelectedTableRow() != null) {

					DataForm dataForm = new DataForm(TopPanel.getSelectedTableRow(), true);

					dataForm.setVisible(true);
					if (dataForm.isCancelled()) {
						return;
					}

					TopPanel.refresh();

				}
			}
		});

		btnDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (TopPanel.getSelectedTableRow() != null) {
					int confirmValue = JOptionPane.showConfirmDialog(MainView.getInstance(),
							"Do you want to delete this?", "Confirm", JOptionPane.OK_CANCEL_OPTION);
					// System.out.println(confirmValue);
					if (confirmValue == 0) {
						Data selectedTableRowData = TopPanel.getSelectedTableRow();
						DataDao.getInstance().delete(selectedTableRowData);
						TopPanel.refresh();
					}
				}
			}
		});

		btnReport.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (DataDao.getInstance().createConnection() != null) {
					report(TopPanel.getSelectedTableRow());
				} else {
					JOptionPane.showMessageDialog(getParent(), "Database not connected!");
				}
			}
		});

		buttonPanel.add(btnAdd);
		buttonPanel.add(btnEdit);
		buttonPanel.add(btnDelete);
		buttonPanel.add(btnReport);

		add(buttonPanel, "center align");

	}

	private void report(Data data) {
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream sourceFileName = classLoader.getResourceAsStream("com/javaee/main/report/InfoReport.jasper");
		List<Data> allData = DataDao.getInstance().getAllData();

		JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(allData);
		Map parameters = new HashMap();
		parameters.put("reportTitle", "Student Info");
		parameters.put("copyRight", "\u00a9" + " Md. Faysal Hasan");
		try {
			JasperPrint printFileName = JasperFillManager.fillReport(sourceFileName, parameters, beanColDataSource);
			if (printFileName != null) {
				// JasperPrintManager.printReport(printFileName, true);
				// JasperExportManager.exportReportToPdfFile(printFileName,
				// "reports/sample_report.pdf");

				JFrame frame = new JFrame("Report");
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame.getContentPane().add(new JRViewer(printFileName));
				frame.setSize(800, 600);
				frame.setLocationRelativeTo(MainView.getInstance());
				frame.setVisible(true);
			}
		} catch (JRException e) {
			e.printStackTrace();
		}
	}

}
