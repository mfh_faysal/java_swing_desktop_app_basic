package com.javaee.main.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DataDao {

	String dbURL = "jdbc:mysql://localhost:3306/sampledb";
	String username = "root";
	String password = "";
	private Connection conn;
	private static DataDao instance;

	public DataDao() {

	}

	public Connection createConnection() {
		try {
			conn = DriverManager.getConnection(dbURL, username, password);
			return conn;
		} catch (SQLException e) {
			return null;
		}
	}

	public void add(Data data) {

		try {
			createConnection();

			if (conn != null) {
				// System.out.println("Connected");

				String sql = "INSERT INTO Users (name, roll, dept, session) VALUES ( ?, ?, ?,?)";

				PreparedStatement statement = conn.prepareStatement(sql);
				statement.setString(1, data.getName());
				statement.setString(2, data.getRoll());
				statement.setString(3, data.getDept());
				statement.setString(4, data.getSession());

				int rowsInserted = statement.executeUpdate();
				if (rowsInserted > 0) {
					// System.out.println("A new user was inserted
					// successfully!");
				}
				conn.close();
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	public void delete(Data data) {
		createConnection();
		String sql = "DELETE FROM Users WHERE id=?";

		try {
			PreparedStatement statement = conn.prepareStatement(sql);
			statement.setInt(1, data.getId());

			int rowsDeleted = statement.executeUpdate();
			if (rowsDeleted > 0) {
				// System.out.println("A user was deleted successfully!");
			}
			conn.close();
		} catch (Exception e) {
		}
	}

	public List<Data> getAllData() {
		createConnection();
		String sql = "SELECT * FROM Users";

		List<Data> dataList = new ArrayList<Data>();

		try {
			Statement statement = conn.createStatement();
			ResultSet result = statement.executeQuery(sql);

			int count = 0;

			while (result.next()) {
				int id = result.getInt("id");
				String name = result.getString("name");
				String roll = result.getString("roll");
				String dept = result.getString("dept");
				String session = result.getString("session");

				// System.out.println(String.format(name, roll, dept, session));

				Data data = new Data();

				data.setId(id);
				data.setName(name);
				data.setRoll(roll);
				data.setDept(dept);
				data.setSession(session);

				dataList.add(data);
			}
			conn.close();
			return dataList;
		} catch (Exception e) {

		}

		return null;
	}

	public void update(Data data) {

		createConnection();
		String sql = "UPDATE Users SET name=?, roll=?, dept=?, session=? WHERE id=?";

		try {
			PreparedStatement statement = conn.prepareStatement(sql);
			statement.setString(1, data.getName());
			statement.setString(2, data.getRoll());
			statement.setString(3, data.getDept());
			statement.setString(4, data.getSession());
			statement.setInt(5, data.getId());

			int rowsUpdated = statement.executeUpdate();
			if (rowsUpdated > 0) {
				// System.out.println("An existing user was updated
				// successfully!");
			}
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static DataDao getInstance() {
		if (instance == null) {
			instance = new DataDao();

		}
		return instance;
	}

}
